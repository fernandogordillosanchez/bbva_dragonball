package com.bbva.pe;

import com.bbva.pe.controllers.DragonballController;
import com.bbva.pe.model.DragonBall;
import com.bbva.pe.service.DragonBallService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = DragonballController.class)
class DragonBallControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DragonBallService dragonBallService;

    private DragonBall[] dragonBalls;

    private DragonBall[] dragonBallsq = new DragonBall[3];


    @BeforeEach
    void setUp() {
        this.dragonBalls = new DragonBall[3];

        dragonBalls[0]=new DragonBall("unknown", "Alive", "Unknown", "Male","5c7c89fb12b25c00177aa150","Frieza","Z","2019-03-04T02:14:19.836Z","/api/character/Frieza","../images/Frieza.jpg",0);
        dragonBalls[1]=new DragonBall("saiyan", "Alive", "Unknown", "Male","5c7c89fb12b25c00177aa150","Frieza","Z","2019-03-04T02:14:19.836Z","/api/character/Frieza","../images/Frieza.jpg",0);
        dragonBalls[2]=new DragonBall("terricola", "Alive", "Unknown", "Male","5c7c89fb12b25c00177aa150","Frieza","Z","2019-03-04T02:14:19.836Z","/api/character/Frieza","../images/Frieza.jpg",0);


    }


    @Test
    void listDragonBallController() throws Exception{
        given(dragonBallService.listALLDragonBall()).willReturn(dragonBalls);

        this.mockMvc.perform(post("/v1.0/DragonBall"))
                .andExpect(status().isOk());
    }


}
