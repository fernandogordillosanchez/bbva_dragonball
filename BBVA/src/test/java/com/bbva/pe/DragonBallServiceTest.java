package com.bbva.pe;
import java.net.URI;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;


@RunWith(SpringRunner.class)
@SpringBootTest()
public class DragonBallServiceTest {



	@Test
	public void listDragonBall() throws Exception {

		RestTemplate restTemplate = new RestTemplate();

		final String baseUrl = "https://dragon-ball-api.herokuapp.com/api/character";
		URI uri = new URI(baseUrl);

		ResponseEntity<String> result = restTemplate.getForEntity(uri, String.class);

		Assert.assertEquals(200, result.getStatusCodeValue());

	}

}
