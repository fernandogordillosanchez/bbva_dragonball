package com.bbva.pe.controllers;

import com.bbva.pe.model.Data;
import com.bbva.pe.model.DragonBall;
import com.bbva.pe.service.DragonBallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/v1.0/DragonBall")
public class DragonballController {

    @Autowired
    DragonBallService dragonBallService;


    @PostMapping()
    public Data listDragonBall() {

        DragonBall[] dragonBall = dragonBallService.listALLDragonBall();

        List<DragonBall> listDragonBall = Arrays.asList(dragonBall);

        Data data = new Data();
        data.setData(listDragonBall);

        return data;

    }

}
