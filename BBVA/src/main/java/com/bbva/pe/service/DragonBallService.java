package com.bbva.pe.service;

import com.bbva.pe.model.DragonBall;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class DragonBallService {


    @Value("${url.dragonBall}")
    private String url;

    private final RestTemplate restTemplate;

    public DragonBallService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }


    public DragonBall[] listALLDragonBall() {
        return this.restTemplate.getForObject(url, DragonBall[].class);
    }


}
